#version 410
#define M_PI 3.14159265358979323846

#define NB_BOUNCE 10
#define EPS 0.001

uniform mat4 mat_inverse;
uniform mat4 persp_inverse;
uniform sampler2D envMap;
uniform vec3 center;
uniform float radius;

uniform bool transparent;
uniform float shininess;
uniform float eta;

in vec4 position;

out vec4 fragColor;



/* return the color from the texture with the direction */
vec4 getColorFromEnvironment(in vec3 direction){

	float theta = acos(direction.z)/(M_PI);
	float phi = atan(direction.y,direction.x)/(2*M_PI) +0.5;
	return texture(envMap, vec2(phi,theta));
}

/*


ior : index of refraction
I : incoming ray
N : normal

*/

float fresnel(vec3 I, vec3 N, float ior){

	float fresnelCoef;
	float tmp;

	float cosi = dot(I, N);
	float etai = 1, etat = ior;
	if (cosi > 0) {
		tmp = etai;
		etai = etat;
		etat = tmp;
	}
	// Compute sini using Snell's law
	float sint = etai / etat * sqrt(max(0.f, 1 - cosi * cosi));
	// Total internal reflection
	if (sint >= 1) {
		fresnelCoef = 1;
	} else {
		float cost = sqrt(max(0.f, 1 - sint * sint));
		cosi = abs(cosi);
		float Rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
		float Rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
		fresnelCoef = (Rs * Rs + Rp * Rp) / 2;
	}

	return fresnelCoef;
}

bool raySphereIntersectFromInside(in vec3 start, in vec3 direction, in float rad, in vec3 cen, out vec3 newPoint) {

    float a = dot(direction,direction);
    float b = -dot(direction,cen-start);
	float c = dot(cen-start,cen-start)-rad*rad;
	float d = b*b - a*c;

	if(d > EPS)	{
		d = sqrt(d);

        float tmp = (-b+d)/a;
        if(tmp > EPS){
            newPoint = start + direction * tmp;
			return true;
        }
	}
	return false;
}


bool raySphereIntersect(in vec3 start, in vec3 direction, in float rad, in vec3 cen, out vec3 newPoint) {


    float a = dot(direction,direction);
	float b = -dot(direction,cen-start);
	float c = dot(cen-start,cen-start)-rad*rad;
	float d = b*b - a*c;

	if(d > EPS)	{
		d = sqrt(d);
		float tmp = (-b-d)/a;

		if(tmp > EPS){ // there is a collision
			newPoint = start + direction * tmp;
			return true;
		}
	}
	return false;
}


// metalic sphere
vec4 getColorReflectionSphere(vec3 eye, vec3 u){

	vec4 color;

	vec3 intersectPoint;

	if(raySphereIntersect(eye, u, radius, center, intersectPoint)){
		vec3 normal = normalize(intersectPoint - center);

		vec3 reflectedVector = normalize(reflect(u, normal));

		color = getColorFromEnvironment(reflectedVector);

	} else {
		color = getColorFromEnvironment(u);
	}

	return color;
}



float getFresnel(vec3 rd, vec3 n, float r0){
    float ndotv = clamp(dot(n, -rd), 0.0,1.0);
    return r0 + (1.0 - r0) * pow(1.0 - ndotv, 5.0);
}


vec4 getColor(vec3 eye, vec3 u, float eta){

    vec3 normal, reflectedVector, refractVector;
    float fresnelCoef, attenuation = 1.0;

	float radiusInnerSphere = radius - 0.1;

	 // INIT

	vec4 resultColor = vec4(0.0,0.0,0.0,1.0);

	vec3 intersectPoint, rayDirection = u, startingPoint = eye;


    if(raySphereIntersect(startingPoint, rayDirection, radius, center, intersectPoint)){ // if it intersect with outer sphere


        normal = normalize(intersectPoint - center);

        reflectedVector = normalize(reflect(rayDirection, normal));
        refractVector = normalize(refract(rayDirection, normal,1/eta));

        fresnelCoef = fresnel(rayDirection, normal, eta);

        attenuation *= 1-fresnelCoef;

        resultColor += getColorFromEnvironment(reflectedVector)*fresnelCoef;

        startingPoint = intersectPoint - normal*EPS;
        rayDirection = refractVector;

        for(int i=1; i<NB_BOUNCE; i++){

            // intersection with the outer sphere
            if(raySphereIntersectFromInside(startingPoint, rayDirection, radius, center, intersectPoint)){
                // resultColor = vec4(0.0,1.0,0.0,1.0);

                normal = normalize(center - intersectPoint);

                reflectedVector = normalize(reflect(rayDirection, normal));
                refractVector = normalize(refract(rayDirection, normal,eta));

                fresnelCoef = fresnel(rayDirection, normal, eta);

                if(fresnelCoef != 1){
                    resultColor += getColorFromEnvironment(refractVector)*attenuation*(1-fresnelCoef);
                }

                startingPoint = intersectPoint + normal*EPS;
                rayDirection = reflectedVector;
                attenuation *= fresnelCoef;

            // inner sphere
            } else if(raySphereIntersect(startingPoint, rayDirection, radiusInnerSphere, center, intersectPoint)){
                // resultColor = vec4(1.0,0.0,0.0,1.0);

                normal = normalize(intersectPoint - center);

                reflectedVector = normalize(reflect(rayDirection, normal));
                fresnelCoef = fresnel(rayDirection, normal, eta);

                startingPoint = intersectPoint + normal*EPS;
                rayDirection = reflectedVector;
                attenuation *= fresnelCoef;

            }



        }



    } else {
        resultColor = getColorFromEnvironment(u);
    }


   resultColor.a = 1;

   return resultColor;
   // return col;



}


void main(void){

    // Step 1: I need pixel coordinates. Division by w?
    vec4 worldPos = position;
    worldPos.z = 1; // near clipping plane
    worldPos = persp_inverse * worldPos;
    worldPos /= worldPos.w;
    worldPos.w = 0;
    worldPos = normalize(worldPos);
    // Step 2: ray direction:
    vec3 u = normalize((mat_inverse * worldPos).xyz);
    vec3 eye = (mat_inverse * vec4(0, 0, 0, 1)).xyz;

	vec4 resultColor;

	// RENDERING

	if(transparent){
		resultColor = getColor(eye, u, eta);
	} else {
		resultColor = getColorReflectionSphere(eye, u);
	}

    fragColor = resultColor;
}
