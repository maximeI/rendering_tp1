#version 410

#define M_PI 3.14159265358979323846

uniform float lightIntensity;
uniform bool blinnPhong;
uniform float shininess;
uniform float roughnessValue;
uniform float eta;
uniform sampler2D shadowMap;

in vec4 eyeVector;
in vec4 lightVector;
in vec4 vertColor;
in vec4 vertNormal;
in vec4 lightSpace;

out vec4 fragColor;

/* mu: index of refraction of material */
float fresnel(float angle, float mu){

    float sinsquared = (1.0-cos(2*angle))/2.0;
    float ci = sqrt(mu*mu - sinsquared);

    float b = cos(angle) + ci;

    if(b == 0.0){
        return 0.001;
    }

    float fs = abs((cos(angle)-ci)/b) * abs((cos(angle)-ci)/b);

    b = mu*mu*cos(angle) + ci;

    if(b == 0.0){
        return 0.001;
    }

    float fp = abs((mu*mu*cos(angle)-ci)/b) * abs((mu*mu*cos(angle)-ci)/b);

    return (fs+fp)/2.0;

}

float blinnPhongFunction(float s, vec3 H, vec3 n, vec3 l, float teta){
    float specularColor = max(dot(n , H), 0);

    if(specularColor != 0.0){
        specularColor = pow(specularColor, s);
        specularColor *= fresnel(dot(H, l), teta);
    }

    return specularColor;
}

float tan2(float angle){
    return (1-cos(2*angle))/(1+cos(2*angle));
}

// alpha = roughness
float G(vec3 e, vec3 normal, vec3 lightDir, vec3 H){

    float vdoth = max(dot(e, H), 0);
    float b = 2.0 * max(dot(normal, H),0);
    float G1 = (b * dot(normal,e))/ vdoth;
    float G2 = (b * dot(normal,lightDir))/ vdoth;
    float G = min(1.0, min(G1,G2));

    return G;

}


float D(float tetaH, float roughness){

    float rSquared = roughness*roughness;
    float r1 = 1.0/(4.0 * rSquared * pow(tetaH, 4.0));
    float r2 = (tetaH*tetaH -1.0)/(rSquared * tetaH * tetaH);

    return (r1 * exp(r2));

}


float cookTorranceFunction(vec3 viewDir, vec3 lightDir, float indexOfRefract, vec3 normal, vec3 e){
    float roughness = roughnessValue;

    vec3 H = normalize(lightDir + viewDir);

    float tetai = dot(H, normal);
    float tetao = dot(H, e);

    float valD = D(max(dot(normal, H),0), roughness);

    float valG = G(e, normal, lightDir, H);



    float returnVal = fresnel(dot(H, lightDir), indexOfRefract) * valD * valG;
    returnVal /= M_PI * dot(normal,lightDir) * dot(normal, e);
    return returnVal;
}

/*
 l: lightVector
 color: vertColor
 e: eyeVector
 n: vertNormal
 lightI: lightIntensity
 s: shininess
*/
vec4 computedColor(vec4 color, vec3 e, vec3 l, vec3 n, float lightI, float s, float teta){
    const float ambientReflectionFactor = 0.2;
    const float diffuseRefletionFactor = 0.5;
    const float specReflectionFactor = 1.2;

    float ambientColor = ambientReflectionFactor;

    float diffuseColor = diffuseRefletionFactor * max(dot(n, l), 0);


    vec3 VL = e + l;
    float nVL = length(VL);

    float specularColor = 0.0;
    if(nVL != 0.0){
        vec3 H = VL / nVL;

        if(blinnPhong){
            specularColor += blinnPhongFunction(s, H, n, l, teta);
        } else {
            specularColor += cookTorranceFunction(e, l, teta, n, e);
        }

    }

    vec4 blinnPhongColor = lightI * color * (ambientColor + diffuseColor + specularColor);

    return blinnPhongColor;
}

void main( void ){

    vec4 e = normalize(eyeVector);
    vec4 l = normalize(lightVector);
    vec4 n = normalize(vertNormal);

     fragColor = computedColor(vertColor, e.xyz, l.xyz, n.xyz, lightIntensity, shininess, eta);
}
